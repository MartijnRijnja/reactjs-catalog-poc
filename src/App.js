import React, { Component } from 'react';
import axios from 'axios';
import BikeGrid from './components/BikeGrid';
import Filters from './components/Filters';
import './css/styles.scss';

export default class App extends Component {
  state = {
    products: [],
    navigation: [],
    url: process.env.REACT_APP_POP_URL
  };

  /**
   * Event handler when clicking a filter
   * @todo: Selection of multiple filters with the same key
   *
   * @param filter
   * @param value
   * @param active
   */
  toggleFilter = (filter, value, active) => {
    const url = new URL(this.state.url);

    if (active) {
      url.searchParams.delete(filter + '[]');
    } else {
      url.searchParams.set(filter + '[]', value);
    }
    this.setState({url: url});
    this.doBikeSearch(url);
  };

  /**
   * Retrieve results after selecting a filter
   *
   * @param url
   */
  doBikeSearch = (url) => {
    // @todo: this.state.url doesn't have the new state yet for some reason, hence the url param
    axios.get(url || this.state.url)
    .then(
      res => {
        this.setState({
          products: this.formatBikeData(res.data.products),
          navigation: res.data.navigation
        });
      }
    )
  };

  formatBikeData(bikes) {
    return bikes.map(bike => (
      {
        name: bike.name,
        image: bike.image,
        price: {
          price: bike.price[0].price
        },
        swatches: {
          frames: this.getSwatches(
              bike.swatches_js.jsonConfig.attributes,
              'pim_itemgenderframe'
          ),
          colors: this.getSwatches(
              bike.swatches_js.jsonConfig.attributes,
              'pim_itemcolorcodeonergb'
          )
        }
      }
    ));
  }

  getSwatches(attributes, code) {
    for (var attr in attributes) {
      if (attributes[attr].code === code) return attributes[attr].options;
    }
  }

  componentDidMount() {
    this.doBikeSearch();
  }

  render () {
    return (
      <div className="page">
        <div className="container">
          <Filters filters={ this.state.navigation } toggleFilter={this.toggleFilter} />
          <BikeGrid bikes={ this.state.products } />
        </div>
      </div>
    )
  };
}
