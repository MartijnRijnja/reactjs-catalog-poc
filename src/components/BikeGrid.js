import React, {Component} from 'react';
import BikeTile from './grid/BikeTile';
import PropTypes from 'prop-types';
import { Flex } from './styled/Layout'

export default class BikeGrid extends Component {
  render() {
    return (
      <Flex wrap="true" width="66%">
        {this.props.bikes.map((bike, index) => (
          <BikeTile key={index} bike={bike} />
        ))}
      </Flex>
    )
  }
}

// PropTypes
BikeGrid.propTypes = {
  bikes: PropTypes.array.isRequired
};
