import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import variables from '../../../css/variables.scss';

export default class Checkbox extends Component {
  render() {
    const {value, count, active} = this.props.item;
    return (
      <FilterCheckbox active={active} onClick={this.props.toggleFilter.bind(this, value, active)}>
        <FilterLabel>{value}</FilterLabel>
        <FilterCount>{count}</FilterCount>
      </FilterCheckbox>
    )
  };
}

const FilterCheckbox = styled.div`
  display: flex;
  cursor: pointer;
  width: 100%;
  padding-bottom: 10px;
  margin: 4px 0;
  
  ::after {
    margin-left: auto;
    content: '';
    display: block;
    width: 18px;
    height: 18px;
    border-radius: 2px;
    border: 1px solid ${variables.grey};
    
    ${props => props.active && `
      width: 14px;
      height: 14px;
      margin-right: 1px;
      box-shadow: 0px 0px 0px 1px ${variables.blue};
      background-color: ${variables.blue};
      border: 2px solid ${variables.white};
    `}
  }
`;

// PropTypes
Checkbox.propTypes = {
  item: PropTypes.object.isRequired
};

// Styles
const FilterLabel = styled.div`
  font-size: 14px;
  margin-right: 10px;
`;

const FilterCount = styled.div`
  font-size: 10px;
  font-weight: bold;
  color: ${variables.grey};
`;
