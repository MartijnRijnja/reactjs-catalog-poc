import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import variables from '../../../css/variables.scss';

export default class Tile extends Component {
  render() {
    const { value, count, active } = this.props.item;
    return (
      <FilterTile active={active} onClick={this.props.toggleFilter.bind(this, value, active)}>
        <FilterLabel >{ value }</FilterLabel>
        <div>{ count }</div>
      </FilterTile>
    )
  };
}

// PropTypes
Tile.propTypes = {
  item: PropTypes.object.isRequired
};

// Styles
const FilterTile = styled.div`
  cursor: pointer;
  width: calc(100%/3 - 20px);
  height: 0;
  text-align: center;
  padding-bottom: calc(100%/3 - 20px);
  float: left;
  margin-right: 10px;
  border-radius: 2px;
  background-color: ${variables.lightgrey};
  
  ${props => props.active && `
    background-color: ${variables.blue};
    color: ${variables.white};
  `}
`;

const FilterLabel = styled.div`
  margin-top: 30%;
  font-weight: bold;
`;
