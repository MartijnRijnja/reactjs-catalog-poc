import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Tile from './items/Tile';
import Checkbox from './items/Checkbox';
import { Flex } from '../styled/Layout';
import { H4 } from '../styled/Typography';

export default class Filter extends Component {
  render() {
    const {template, attribute, title, items} = this.props.filter;
    return (
      <React.Fragment>
        <H4>{title}</H4>
        <Flex wrap="true" marginBottom='20px'>
          {items ? items.map((item, index) => (
            template === 'tiles'
              ? <Tile key={index} item={item} toggleFilter={this.props.toggleFilter.bind(this, attribute)} />
              : <Checkbox key={index} item={item} toggleFilter={this.props.toggleFilter.bind(this, attribute)} />
          )) : ''}
        </Flex>
      </React.Fragment>
    )
  };
}

// PropTypes
Filter.propTypes = {
  filter: PropTypes.object.isRequired
};
