import styled from 'styled-components';
import variables from '../../css/variables.scss';

export const H4 = styled.h4`
  color: ${variables.blue};
  letter-spacing: 1.5px;  
  text-transform: uppercase;
  font-weight: bold;
  margin-bottom: 10px;
`;
