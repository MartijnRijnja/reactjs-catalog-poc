import styled from 'styled-components';
import variables from '../../../css/variables.scss';

export const Tile = styled.div`
  padding: 1.5rem;
  border-radius: 2px;
  width: 50%
`;

export const TileInfo = styled.div`
  padding: 3rem;
  background-color: ${variables.lightgrey};
`;

export const FrameIcon = styled.div`
  text-align: center;
  height: 32px;
  width: 33%;
  marginBottom: -1px;    
`;
