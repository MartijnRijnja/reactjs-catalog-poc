import styled, { css } from 'styled-components';

export const Flex = styled.div`
  display: flex;
  
  ${props => props.marginBottom && `
    margin-bottom: ${props.marginBottom};
  `}

  // Variation 1  
  ${props => props.width && `
    width: ${props.width};
  `}
  
  // Variation 2 (does the same..)
  ${props => props.height && css`
    height: ${props => props.height};
  `}
  
  ${props => props.wrap && css`
    flex-wrap: wrap;
  `}
`;

export const Sidebar = styled.div`
  width: calc(100%/3);
  padding-right: 30px;
`;
