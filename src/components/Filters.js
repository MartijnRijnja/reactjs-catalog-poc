import React, {Component} from 'react';
import Filter from './filters/Filter';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Sidebar } from '../components/styled/Layout';
import variables from '../css/variables.scss';

export default class Filters extends Component {
  render() {
    return (
      <Sidebar>
        <H1>Filters</H1>
        {this.props.filters.map((filter, index) => (
          <Filter key={index} filter={filter} toggleFilter={this.props.toggleFilter} />
        ))}
      </Sidebar>
    )
  }
}

// PropTypes
Filters.propTypes = {
  filters: PropTypes.array.isRequired
};

// Styles
const H1 = styled.h1`
  border-bottom: 1px solid ${variables.lightgrey};
  margin-bottom: 20px;
`;
