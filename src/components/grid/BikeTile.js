import React, { Component } from 'react';
import PropTypes from 'prop-types';
import variables from '../../css/variables.scss';
import { LazyLoadImage } from 'react-lazy-load-image-component';
import { Tile, TileInfo, FrameIcon } from '../styled/Pages/Catalog';
import styled from "styled-components";

export default class BikeTile extends Component {
  render() {
    const { name, image, price, swatches } = this.props.bike;
    return (
      <Tile>
        <TileInfo>
          <div style={{ fontSize: '1.4rem', marginBottom: '8px' }}>{ name }</div>
          <div>
            <span style={{ fontSize: '1.2rem', marginRight: '5px' }}>Vanaf</span>
            <span style={{ fontSize: '1.5rem', fontWeight: '500' }}>{ price.price }</span>
          </div>
          <LazyLoadImage
              alt=''
              src={image}
              threshold="1"
          />
          <div style={{ display: 'flex', borderBottom: '1px solid ' + variables.grey }}>
            {swatches.frames.map((frame, index) => (
              <FrameIcon key={index}>
                <div className="icon-frame-f" />
              </FrameIcon>
            ))}
          </div>
          <TileIcons>
            {swatches.colors.map((color, index) => (
              <div key={index} className="color-swatch" style={{ backgroundColor: 'rgb(' + color.label + ')' }} />
            ))}
          </TileIcons>
          <div style={{ display: 'flex' }}>
            <button style={{ flexGrow: '1' }}>Compare</button>
            <button style={{ flexGrow: '1' }} className="blue">View</button>
          </div>
        </TileInfo>
      </Tile>
    )
  };
}

// PropTypes
BikeTile.propTypes = {
  bike: PropTypes.object.isRequired
};


// Styles
const TileIcons = styled.div`
  display: flex;
  margin: 15px 0;
`;
