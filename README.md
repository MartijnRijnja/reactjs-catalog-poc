## ReactJS Catalog POC

- Set the POP endpoint (`REACT_APP_POP_URL`) in .env
- Make sure you have some kind of CORS plugin to allow localhost to access your Magento environment.
- Run `npm start`
